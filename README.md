# Gulp boilerplate

## Installation

#### Install Node & Bower packages:

```npm install && bower install```

If you don't need any Bower packages, you will have to turn off the font task.


## Usage

#### Development

`npm start`

#### Production

`npm run build`
